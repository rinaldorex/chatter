# Have the config for the application over here.
import logging

SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:<user>@<host>/<dbname>'

MONGODB_URI = 'mongodb://<host>/<dbname>'
MONGODB_NAME = 'chatter'
MONGOATLAS_API_KEY = 'SomeApiKey'
MAILGUN_API_KEY = 'MailgunApiKey'
MAILGUN_BASE_URL = 'https://api.mailgun.net/v3/<orgname>'
MAILGUN_EMAIL_SUFFIX = '/messages'  # Default mailgun messaging route

TRAINING_DATA_DIR = 'app/data/training_data'

# If they don't specify who they want to mail, defaults to the support
DEFAULT_SUPPORT_MAIL = 'tony_stark@starkindustries.com'
DEFAULT_SALES_MAIL = 'obadiah_stain@starkindustries.com'
DEFAULT_MARKETING_MAIL = 'pepper_potts@starkindustries.com'

APP_NAME = 'chatter'
APP_PORT = 3000
LOG_DIR = 'logs'
SECRET_KEY = 'randomsecrettykey'

# Configuration for the python logging module
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': logging.DEBUG,
        'handlers': ['console', 'file'],
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': logging.DEBUG,
            'formatter': 'detailed',
            'stream': 'ext://sys.stdout',
        },
        'file': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': logging.DEBUG,
            'formatter': 'detailed',
            # 'filename': '' + APP_NAME.lower() + '.log',
            'filename': '{}/{}.log'.format(LOG_DIR, APP_NAME.lower()),
            'mode': 'a',
            'maxBytes': 10485760,
            'backupCount': 5,
        }
    },
    'formatters': {
        'detailed': {
            'format': ('%(asctime)s %(name)-17s line:%(lineno)-4d '
                        '%(levelname)-8s %(message)s')
        },
        'with_filter': {
            'format': ('%(asctime)s %(name)-17s line:%(lineno)-4d '
                        '%(levelname)-8s User: %(user)-35s %(message)s')
        },
    },
}