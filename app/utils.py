"""
All the utilities for the chatter should be present here.

Currently, the Mailgun mailer and the critical Bot is implemented.
"""

import logging
import requests
import random
import os

from datetime import datetime
from flask import session
from textblob import TextBlob
from textblob.classifiers import NaiveBayesClassifier

from .models import BaseQuery, BaseResponse
from app import nlp

logger = logging.getLogger(__name__)

try:
    from app.config import (
        DEFAULT_MARKETING_MAIL,
        DEFAULT_SALES_MAIL,
        DEFAULT_SUPPORT_MAIL,
        MAILGUN_API_KEY,
        MAILGUN_BASE_URL,
        MAILGUN_EMAIL_SUFFIX
    )
except ModuleNotFoundError as e:
    logger.warning('Config file not found. Fetching from env')
    DEFAULT_SUPPORT_MAIL = os.getenv('DEFAULT_SUPPORT_MAIL')
    DEFAULT_SALES_MAIL = os.getenv('DEFAULT_SUPPORT_MAIL')
    DEFAULT_MARKETING_MAIL = os.getenv('DEFAULT_SUPPORT_MAIL')
    MAILGUN_API_KEY = os.getenv('MAILGUN_API_KEY')
    MAILGUN_BASE_URL = os.getenv('MAILGUN_BASE_URL')
    MAILGUN_EMAIL_SUFFIX = os.getenv('MAILGUN_EMAIL_SUFFIX')

    MONGODB_URI = os.getenv('MONGODB_URI')
    MONGODB_NAME = os.getenv('MONGODB_NAME')
    TRAINING_DATA_DIR = os.getenv('TRAINING_DATA_DIR')


class BotMailer:
    """
    This is the mailer that has the classifier initialized.
    """
    def __init__(self, train_query_data=None, train_resp_data=None):

        if not train_query_data:
            train_query_data = [
                (data['text'], data['category']) for data in BaseQuery.objects
            ]

        # Primary classifier of the bot
        self.cl = NaiveBayesClassifier(train_query_data)

        # TODO: Poor way of handling possible queries if this if for any other use cases
        # You can improve this by storing these in the DB.
        # Since the current use case is singular : Mailing via conversation,
        # this suffices
        self.to_queries = [
            'Who do you want to contact?(Marketing manager, Sales team, etc)',
            'Shall I mail the sales team?',
            'Shall I mail the Marketing team?',
        ]

        # To identify if the user should probably talk to the sales team.
        self.sales_queries = [
            nlp('I need to talk to the sales team'),
            nlp('I wanna discuss the pricing'),
            nlp('Will I get any discount for the same?'),
        ]

        # To identify if the user should probably talk to the marketing team.
        self.marketing_queries = [
            nlp('Can I work with the organization in a custom product?'),
            nlp('I want a customized product built'),
            nlp('I need to discuss enterprise solutions'),
        ]

        # This is a set of possible queries our bot can enquire for subject of mail
        self.subject_queries = [
            'What would you like to enquire about? (Max: 70)',
            'What do you want to know more about? (Max: 70)'
        ]

        # This is a set of possible queries our bot can enquire for message of mail
        self.message_queries = [
            "Please compose your message. (Max: 200)"
        ]

        # This is the final contact details of mail
        # Why do we need this? Because this helps in storing the user data even if they
        # cancel the email flow if they have other things in mind.
        # Maybe we can use this later for sales. ;)
        # But of course, abide by GDPR.

        self.contact_data = {
            'to': None,
            'subject': None,
            'message': None,
            'from': None,
        }

    def respond(self, category):
        # TODO: Change BaseQuery to BaseResponse once you have enough data
        # Warning right now only uses greeting messages from Query instead of response

        viable_responses = BaseResponse.objects(category=category)
        if category == 'GREETING':
            while True:
                resp = random.choice(viable_responses).text
                current_hour = int(datetime.utcnow().hour)
                if str(resp).lower().__contains__('noon'):
                    if 12 < current_hour < 16:
                        return resp
                    else:  # To skip other checks.
                        continue
                elif str(resp).lower().__contains__('evening'):
                    if 15 < current_hour < 24:
                        return resp
                    else:
                        continue
                elif str(resp).lower().__contains__('morn'):
                    if 0 < current_hour < 12:
                        return resp
                    else:
                        continue
                else:
                    return resp

    def classify(self, query):
        """
        Returns the category of the response after classification

        :param str query: User query
        :return str: Category predicted/inferred by the classifier
        """
        predicted_category = str(self.cl.classify(query))
        return predicted_category

    def initiate_contact(self, query):
        """
        This initiates the contact flow.

        :return:
        """
        # Initialize a session object for the contact
        if 'contact_request' not in session:
            session['contact_request'] = {
                'to': None,
                'subject': None,
                'message': None,
                'from': None,
            }

        # To maintain/track the flow process
        session['contact_flow'] = True

        # The bot is very basic. In the basic use case, we need at least the last
        # couple messages to respond accordingly.
        # TODO: Further development of the bot can handle an entire history of the chat.
        last_user_resp = None  # Last response of the user
        last_bot_resp = None  # Last response of the bot

        if session.get('messages'):
            if len(session.get('messages')) > 1:
                last_bot_resp = session.get('messages')[-2]
                last_user_resp = session.get('messages')[-1]
            elif len(session.get('messages')) > 0:
                last_user_resp = session.get('messages')[-1]
            else:
                last_user_resp = None
                last_bot_resp = None
        # What if the user want's to cancel mail sending in the middle of the pipeline?
        # We analyse the text (basic) to see if they want to cancel and reset the flow
        if nlp(str(query).lower()).similarity(nlp('cancel')) > 0.8:
            # Cancel the mailing flow.
            session['contact_flow'] = False
            # Reset the session variable
            # TODO: For future use case, we can store this directly on the db after
            # sanitizing the data.
            if 'contact_request' in session:
                session.pop('contact_request')
            return str("Alright, What else do you want to know?")
        if last_bot_resp in self.to_queries:
            if nlp(query).similarity(nlp('no')) > 0.6:
                # This means the user response was negative
                logger.debug('Similarity to no: {}'.format(nlp(query).similarity(nlp(
                    'Yes'))))
                return random.choice(self.to_queries)

            # Until the session's to is filled.
            while not session['contact_request']['to']:
                # Identifying who they want to send the mail to.
                for q in self.sales_queries:
                    if nlp(query).similarity(q) > 0.5:
                        # Sales team
                        logger.debug('Similarity to sales: {}'.format(nlp(
                            query).similarity(nlp('Yes'))))
                        session['contact_request']['to'] = DEFAULT_SALES_MAIL
                        break
                for q in self.marketing_queries:
                    if nlp(query).similarity(q) > 0.5:
                        # Marketing team
                        logger.debug('Similarity to marketing: {}'.format(
                                     nlp(query).similarity(nlp('Yes'))))
                        session['contact_request']['to'] = DEFAULT_MARKETING_MAIL
                        break
                # If neither match, send a generic mail.
                session['contact_request']['to'] = DEFAULT_SUPPORT_MAIL

        elif last_bot_resp in self.message_queries:
            # Setting the session's message
            session['contact_request']['message'] = query
        elif last_bot_resp in self.subject_queries:
            # Setting the session's subject
            session['contact_request']['subject'] = query
        elif last_bot_resp == 'Please provide your email ID':
            # Bot has requested email ID of user.
            # TODO: Validate email
            session['contact_request']['from'] = query
        else:
            # TODO: Update this to handle exceptions from normal convo
            pass

        # To fill the contact request during the flow.
        for key, value in dict(session['contact_request']).items():

            if (session['contact_request']['to']
                    and session['contact_request']['from']
                    and session['contact_request']['message']
                    and session['contact_request']['subject']):
                # Control reaches here when everything is available for mailing.
                # Make sure to clear out the session
                self.contact_data = session.pop('contact_request')
                session['contact_request'] = {
                    'to': None,
                    'subject': None,
                    'message': None,
                    'from': None,
                }
                # Resetting contact flow flag
                session['contact_flow'] = False
                mg = MailGunner()
                try:
                    mg.send_mail(
                        from_=self.contact_data['from'],
                        to=self.contact_data['to'],
                        subject=self.contact_data['subject'],
                        message=self.contact_data['message'],
                    )
                except KeyError as e:
                    logger.error('Some value for request is missing : {}'.format(e))
                    return str('Sorry for the inconvenience, Unable to mail your '
                               'request')

                # Upon successfull sending. :)
                return str("Thank you, we'll contact you back soon")
            if not value:
                if key == 'to':
                    # Fetch from to_queries
                    return random.choice(self.to_queries)
                elif key == 'subject':
                    # Fetch from message queries
                    return str(random.choice(self.subject_queries))
                elif key == 'from':
                    # Requester mail.
                    return str('Please provide your email ID')
                else:
                    # Message
                    return str(random.choice(self.message_queries))


class MailGunner:
    """
    All mailgun based methods should be handled by this class
    """

    def __init__(self):
        """
        Initialize the mailgun client with the proper api key
        """
        self.auth = ("api",  MAILGUN_API_KEY)

    def send_mail(self, from_, subject, message, to=None):
        """
        Helper to send mail via Mailgun api.

        :param from_: The sender's mail.
        :param to: Defaults to primary support mail
        :param subject: The subject message
        :param message: The body of the mail
        :return:
        """
        if not to:
            # Fall back to common support mail.
            to = DEFAULT_SUPPORT_MAIL
        x = requests.post(
            MAILGUN_BASE_URL + MAILGUN_EMAIL_SUFFIX,
            auth=self.auth,
            data={
                'from': from_,
                'to': to,
                'subject': subject,
                'html': message,
            }
        )
        logger.info('Sent mail: {}'.format(x))
        return
