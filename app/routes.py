"""
All the routes for the application are defined here.

Note: For API centric expansion of the app, have resources instead of routes.
"""
import logging

from app import app
from collections import deque
from flask import render_template
from flask import request, session


logger = logging.getLogger(__name__)


@app.route('/')
@app.route('/home')
def index():
    """
    Just the homepage. App's minimal ;)
    :return:
    """
    return render_template('index.html')


@app.route('/chat', methods=['GET', 'POST'])
def chat():
    """
    This is the route that handles all the chat for now.

    :return: Rendered template for the chat page
    """

    # Importing here to avoid circular import
    from . import db, bot

    # User query
    query = request.form.get('query')

    if 'messages' not in session:
        # List of old messages. Always ordered(Because it matters) :)
        session['messages'] = []

    # A flag to set if the contact flow should continue.
    # Since we don't have other ways to track it, a flag would do.
    contact_flag = session.get('contact_flow', False)

    if query:
        session['messages'].append(query)

        # Predict the category of the user query based on which response is decided
        query_category = str(bot.classify(query=query))

        if query_category == 'GREETING' and not contact_flag:
            resp = bot.respond(category=query_category)
        else:
            if query_category == 'CONTACT_REQUEST' or contact_flag:
                # This initiates the contact flow pipeline
                resp = bot.initiate_contact(query)
            else:
                logger.error('Unmatched query category. Please look into it')
                resp = str("Sorry, I don't know much about it(Yet)")

        session['messages'].append(resp)

        if len(session.get('messages')) > 10:
            message_queue = deque(session['messages'])
            # Just for some aesthetics. Throwing away past messages is a poor way
            # to handle conversational bots. History is important to fetch proper
            # accurate context and can be used for further analytics.
            # TODO: Improve this by adding the old messages to the db.
            # Problem: We need to sanitize the input prior to storing in the db.
            message_queue.popleft()  # Remove old messages
            session['messages'] = list(message_queue)  # Reassign
        return render_template(
            'index.html', response=resp, messages=session.get('messages'))
    else:
        return render_template('index.html')


@app.route('/api/v1/chat', methods=['POST'])
def chat_api():
    """
    TODO:
    This is the API endpoint for chat in and out

    This can be implemented for API centric chat bot, which will perform far better and
    will enable modular code.
    :return:
    """
    if request.method == 'POST':
        data = request.get_json()
