## Chatter
###### A simple minimal demonstration of a chat bot

The purpose of the bot is to demostrate the ability to handle sending of mails by itself gathering the context from the user conversation.

##### Use case:
    - Handle basic welcome/greeting message
    - Understand if the user wants to contact/mail someone
    - Gather predictive context from user queries.
        - (Whether they want a specific contact or a generic contact)
    - Able to handle edge cases
        - What if the user wants to cancel the flow?
        - (To be updated)


---

##### Overview

###### Tech Stack
- Flask
- Mailgun (For sending mails
- Spacy (For hardcore NLP analysis)
    - NLTK (Required for spacy)
- Textblob (For simple classification/NLP)
- MongoEngine (ORM for mongodb)
- Pandas (For fetching/training data)
- Scikit-learn (_Future advanced ML use cases_)
---
- We use flask to demostrate the bot's basic use case.
- Mailgun is used for transactional mails that are to be sent. In the future, we can use full-fledged mailing _services_ like mailchimp for automatically generating sales report/pricing chart to be sent to the user directly without having to contact the personnal.
- Spacy is the primary NLP lib the bot is going to use. However it is heavy for such a singular use case. We may upgrade/enhance the bot to handle advanced/better predictive conversational building if we use spacy with SciKit-learn's ML based classifiers (eg: Multinominal Naive Bayes)
- Textblob is used for a simple naivebayes classification of the text. Remember, since it's a naivebayes classification, we can't narrow down on the accuracy if we have huge datasets. Spacy can be used for that purpose.
- Mongoengine is used as the data store for it's simplicity and schema-free design. (Because we can easily upgrade the future functions of the bot)
- Pandas is used for simply seeding the database with training data, however in the future, for huge datasets, this will be a plus.


---
###### Note before using
1. Clone the repo.
2. The `config.py` is important to configure the application.
    - Take a look at the config_example.py (config.py is _git_ ignored) for help.
3. Install the dependencies via `pip install -r requirements.txt`
    _- Make sure you activate a virtualenv for better isolation_
4. Run `flask seed` to seed the base data to the mongodb.
5. Run `python run.py` to run the development server.

#### Using.
You can simply talk to the bot to see how it responds.
Please note that it is pretty naive as of now, and can't handle complicated sentences. :)

This was designed to talk to the user and automatically decide who it want's to send the mail to.

##### DEMO:
You can check out the demo of the app at:

[Chat bot for mailing](https://nlp-chat-mailer.herokuapp.com/)

###### Config
For the configuration, you'll need the following keys at least to check the mailing function.

For a basic view. (You can check out the config_example.py for the full config options)

```
MAILGUN_API_KEY = 'MailgunApiKey'
MAILGUN_BASE_URL = 'https://api.mailgun.net/v3/<orgname>'
MAILGUN_EMAIL_SUFFIX = '/messages'  # Default mailgun messaging route
```

The following to set up your db(mongodb)
```
MONGODB_URI = 'mongodb://<host>/<dbname>'
MONGODB_NAME = 'chatter'
```

The following configures the different mails/people the email will be sent to (Will be decided by the bot based on user response)

```
# If they don't specify who they want to mail, defaults to the support
DEFAULT_SUPPORT_MAIL = 'rinaldorexg@gmail.com'
DEFAULT_SALES_MAIL = 'rinaldorexg@gmail.com'
DEFAULT_MARKETING_MAIL = 'rinaldorexg@gmail.com'
```

##### Known bugs
- No confirmation before sending email.
- Uses session for the demo. New instance only if you clear the session.