from app import app
from app import config
from app.config import APP_PORT

if __name__ == '__main__':
    app.run(debug=True, port=APP_PORT, host='localhost')
